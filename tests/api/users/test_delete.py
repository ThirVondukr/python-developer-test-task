import httpx
import pytest
from fastapi import FastAPI
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import User

pytestmark = [pytest.mark.anyio]

_route_name = "users_delete"


@pytest.mark.parametrize(
    "phone_number",
    [
        "7" + "0" * 11,
    ],
)
async def test_not_found(
    fastapi_app: FastAPI,
    http_client: httpx.AsyncClient,
    phone_number: str,
) -> None:
    response = await http_client.delete(
        fastapi_app.url_path_for(_route_name, phone_number=phone_number),
    )
    assert response.status_code == httpx.codes.OK.value


async def test_delete(
    fastapi_app: FastAPI,
    http_client: httpx.AsyncClient,
    user: User,
    session: AsyncSession,
) -> None:
    response = await http_client.delete(
        fastapi_app.url_path_for(_route_name, phone_number=user.phone_number),
    )
    assert response.status_code == httpx.codes.OK.value
    result = await session.execute(select(User).where(User.id == user.id))
    assert result.scalar() is None
