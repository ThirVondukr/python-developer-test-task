import functools

import httpx
import pytest
from fastapi import FastAPI
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from api.users.schema import UserCreateSchema
from core.fields import PhoneNumber
from db.models import Country, User
from tests.utils import cyrillic_alphabet, random_phone, random_str

pytestmark = [pytest.mark.anyio]

_route_name = "users_create"


async def _assert(
    schema: UserCreateSchema,
    country: Country | None,
    user: User | None,
    response: httpx.Response,
) -> None:
    assert country is not None

    assert user is not None
    assert user.country_code == country.id
    for key, value in schema.dict(exclude={"country"}).items():
        assert getattr(user, key) == value

    assert response.status_code == httpx.codes.OK.value
    assert response.json() == {
        "country": schema.country,
        "countryCode": country.id,
        "email": schema.email,
        "name": schema.name,
        "patronymic": schema.patronymic,
        "phoneNumber": schema.phone_number,
        "surname": schema.surname,
    }


@pytest.fixture
def schema() -> UserCreateSchema:
    _str_partial = functools.partial(
        random_str,
        alphabet=cyrillic_alphabet,
        length=50,
    )
    return UserCreateSchema(
        name=_str_partial(),
        surname=_str_partial(),
        patronymic=_str_partial(),
        phone_number=random_phone(),
        email="test@example.com",
        country=_str_partial(),
    )


async def test_create(
    session: AsyncSession,
    fastapi_app: FastAPI,
    http_client: httpx.AsyncClient,
    schema: UserCreateSchema,
) -> None:
    response = await http_client.post(
        fastapi_app.url_path_for(_route_name),
        json=schema.dict(),
    )
    country = (
        await session.execute(
            select(Country).where(Country.name == schema.country),
        )
    ).scalar()
    assert country is not None

    user = (
        await session.execute(
            select(User).where(User.phone_number == schema.phone_number),
        )
    ).scalar()
    await _assert(
        schema=schema,
        response=response,
        country=country,
        user=user,
    )


async def test_update(
    session: AsyncSession,
    fastapi_app: FastAPI,
    http_client: httpx.AsyncClient,
    schema: UserCreateSchema,
    user: User,
) -> None:
    schema.phone_number = PhoneNumber(user.phone_number)
    response = await http_client.post(
        fastapi_app.url_path_for(_route_name),
        json=schema.dict(),
    )
    country = (
        await session.execute(
            select(Country).where(Country.name == schema.country),
        )
    ).scalar()
    await session.refresh(user)
    await _assert(schema=schema, response=response, country=country, user=user)
