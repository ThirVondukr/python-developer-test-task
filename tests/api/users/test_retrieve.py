import uuid

import httpx
import pytest
from fastapi import FastAPI

from db.models import User
from tests.utils import random_phone

pytestmark = [pytest.mark.anyio]

_route_name = "users_retrieve"


@pytest.mark.parametrize(
    "phone_number",
    [
        "random-id",
        str(uuid.uuid4()),
        " ",
        "1" * 13,
    ],
)
async def test_invalid_number(
    fastapi_app: FastAPI,
    http_client: httpx.AsyncClient,
    phone_number: str,
) -> None:
    response = await http_client.get(
        fastapi_app.url_path_for(_route_name, phone_number=phone_number),
    )
    assert response.status_code == httpx.codes.UNPROCESSABLE_ENTITY.value


async def test_not_found(
    fastapi_app: FastAPI,
    http_client: httpx.AsyncClient,
) -> None:
    phone_number = random_phone()
    response = await http_client.get(
        fastapi_app.url_path_for(_route_name, phone_number=phone_number),
    )
    assert response.status_code == httpx.codes.NOT_FOUND.value


async def test_retrieve(
    fastapi_app: FastAPI,
    http_client: httpx.AsyncClient,
    user: User,
) -> None:
    response = await http_client.get(
        fastapi_app.url_path_for(_route_name, phone_number=user.phone_number),
    )
    assert response.status_code == httpx.codes.OK.value
    assert response.json() == {
        "name": user.name,
        "surname": user.surname,
        "patronymic": user.patronymic,
        "phoneNumber": user.phone_number,
        "email": user.email,
        "country": user.country.name,
        "countryCode": user.country.id,
    }
