import random
import string
from collections.abc import Sequence

_alphabet = string.ascii_letters + string.digits
cyrillic_alphabet = "".join(chr(i) for i in range(ord("а"), ord("я") + 1))


def random_str(length: int, alphabet: Sequence[str] = _alphabet) -> str:
    return "".join(random.choices(alphabet, k=length))


def random_phone() -> str:
    return "7" + random_str(11, alphabet=string.digits)
