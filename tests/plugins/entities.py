import random

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import Country, User
from tests.utils import random_phone, random_str


@pytest.fixture
async def user(session: AsyncSession, country: Country) -> User:
    user = User(
        name=random_str(50),
        surname=random_str(50),
        patronymic=None,
        phone_number=random_phone(),
        email=random_str(50),
        country=country,
    )
    session.add(user)
    await session.flush()
    return user


@pytest.fixture
async def country(session: AsyncSession) -> Country:
    country = Country(
        id=random.randint(1, 1000),
        name=random_str(length=20),
    )
    session.add(country)
    await session.flush()
    return country
