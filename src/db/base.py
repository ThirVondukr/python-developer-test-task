from datetime import datetime
from typing import Annotated

from sqlalchemy import DateTime, String
from sqlalchemy.orm import DeclarativeBase

str_50 = Annotated[str, 50]
str_email = Annotated[str, 320]
str_phone = Annotated[str, 12]


class Base(DeclarativeBase):
    type_annotation_map = {
        str_50: String(50),
        str_email: String(320),
        str_phone: String(12),
        datetime: DateTime(timezone=True),
    }


Base.metadata.naming_convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}
