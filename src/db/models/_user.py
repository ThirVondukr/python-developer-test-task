import uuid
from datetime import datetime
from uuid import UUID

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from core.datetime import utc_now
from db import Base
from db.base import str_50, str_email, str_phone
from db.models._countries import Country


class User(Base):
    __tablename__ = "users"

    id: Mapped[UUID] = mapped_column(primary_key=True, default=uuid.uuid4)
    name: Mapped[str_50]
    surname: Mapped[str_50]
    patronymic: Mapped[str_50 | None]
    phone_number: Mapped[str_phone] = mapped_column(index=True, unique=True)
    email: Mapped[str_email | None]
    country_code: Mapped[int] = mapped_column(ForeignKey("country.id"))
    country: Mapped[Country] = relationship()

    date_created: Mapped[datetime] = mapped_column(default=utc_now)
    date_updated: Mapped[datetime] = mapped_column(default=utc_now, onupdate=utc_now)
