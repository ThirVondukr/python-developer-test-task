from fastapi import FastAPI

from . import users
from .middleware import CommitSessionMiddleware


def create_app() -> FastAPI:
    app = FastAPI()

    app.include_router(users.router)
    app.add_middleware(CommitSessionMiddleware)

    @app.get("/health")
    async def healthcheck() -> None:
        return None

    return app
