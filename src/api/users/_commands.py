from fastapi import Depends

from api.users import _mappers
from api.users.schema import UserCreateSchema, UserSchema
from core.dto import UserCreateDto
from core.services import CountryService, UserService


class UserCreateCommand:
    def __init__(
        self,
        user_service: UserService = Depends(),
        country_service: CountryService = Depends(),
    ) -> None:
        self._user_service = user_service
        self._country_service = country_service

    async def execute(self, schema: UserCreateSchema) -> UserSchema:
        country = await self._country_service.suggest_country(
            country_name=schema.country,
        )
        dto = UserCreateDto(
            name=schema.name,
            surname=schema.surname,
            patronymic=schema.patronymic,
            phone_number=schema.phone_number,
            email=schema.email,
            country=country,
        )
        user = await self._user_service.create_or_update(dto=dto)
        return _mappers.map_user_to_schema(user)
