from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.responses import Response

from core.fields import PhoneNumber
from core.services import UserService

from . import _mappers
from ._commands import UserCreateCommand
from .schema import UserCreateSchema, UserSchema

router = APIRouter(
    prefix="/users",
    tags=["users"],
)


@router.get("/{phone_number}")
async def users_retrieve(
    phone_number: PhoneNumber,
    user_service: UserService = Depends(),
) -> UserSchema:
    user = await user_service.retrieve(phone_number=phone_number)
    if user is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

    return _mappers.map_user_to_schema(user)


@router.post("/")
async def users_create(
    schema: UserCreateSchema,
    command: UserCreateCommand = Depends(),
) -> UserSchema:
    return await command.execute(schema=schema)


@router.delete("/{phone_number}")
async def users_delete(
    phone_number: PhoneNumber,
    service: UserService = Depends(),
) -> Response:
    await service.delete(phone_number=phone_number)
    return Response()
