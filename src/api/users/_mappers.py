from db.models import User

from .schema import UserSchema


def map_user_to_schema(model: User) -> UserSchema:
    return UserSchema(
        name=model.name,
        surname=model.surname,
        patronymic=model.patronymic,
        phone_number=model.phone_number,
        email=model.email,
        country=model.country.name,
        country_code=model.country_code,
    )
