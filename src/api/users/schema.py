from pydantic import EmailStr, Field

from api.schema import BaseSchema
from core.fields import PhoneNumber, cyrillic_str_regex


class UserSchema(BaseSchema):
    name: str
    surname: str
    patronymic: str | None
    phone_number: str
    email: str | None
    country: str
    country_code: int


class UserCreateSchema(BaseSchema):
    name: str = Field(max_length=50, regex=cyrillic_str_regex)
    surname: str = Field(max_length=50, regex=cyrillic_str_regex)
    patronymic: str | None = Field(max_length=50, regex=cyrillic_str_regex)
    phone_number: PhoneNumber
    email: EmailStr
    country: str = Field(max_length=50, regex=cyrillic_str_regex)
