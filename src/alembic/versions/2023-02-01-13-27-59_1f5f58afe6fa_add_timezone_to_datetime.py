"""Add timezone to datetime

Revision ID: 1f5f58afe6fa
Revises: 0a855967e997
Create Date: 2023-02-01 13:27:59.954599

"""
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from alembic import op

# revision identifiers, used by Alembic.
revision = "1f5f58afe6fa"
down_revision = "0a855967e997"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column(
        "users",
        "date_created",
        existing_type=postgresql.TIMESTAMP(),
        type_=sa.DateTime(timezone=True),
        existing_nullable=False,
    )
    op.alter_column(
        "users",
        "date_updated",
        existing_type=postgresql.TIMESTAMP(),
        type_=sa.DateTime(timezone=True),
        existing_nullable=False,
    )


def downgrade() -> None:
    op.alter_column(
        "users",
        "date_updated",
        existing_type=sa.DateTime(timezone=True),
        type_=postgresql.TIMESTAMP(),
        existing_nullable=False,
    )
    op.alter_column(
        "users",
        "date_created",
        existing_type=sa.DateTime(timezone=True),
        type_=postgresql.TIMESTAMP(),
        existing_nullable=False,
    )
