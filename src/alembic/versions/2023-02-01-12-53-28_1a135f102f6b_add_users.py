"""Add users

Revision ID: 1a135f102f6b
Revises:
Create Date: 2023-02-01 12:53:28.706660

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = "1a135f102f6b"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "users",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("name", sa.String(length=50), nullable=False),
        sa.Column("surname", sa.String(length=50), nullable=False),
        sa.Column("patronymic", sa.String(length=50), nullable=True),
        sa.Column("phone_number", sa.String(length=12), nullable=False),
        sa.Column("email", sa.String(length=320), nullable=True),
        sa.Column("country", sa.String(length=50), nullable=False),
        sa.Column("date_created", sa.DateTime(), nullable=False),
        sa.Column("date_updated", sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_users")),
    )
    op.create_index(
        op.f("ix_users_phone_number"),
        "users",
        ["phone_number"],
        unique=False,
    )


def downgrade() -> None:
    op.drop_index(op.f("ix_users_phone_number"), table_name="users")
    op.drop_table("users")
