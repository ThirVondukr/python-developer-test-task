"""Add country

Revision ID: 0a855967e997
Revises: 1a135f102f6b
Create Date: 2023-02-01 13:26:46.524641

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = "0a855967e997"
down_revision = "1a135f102f6b"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "country",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_country")),
    )
    op.add_column("users", sa.Column("country_code", sa.Integer(), nullable=False))
    op.create_foreign_key(
        op.f("fk_users_country_code_country"),
        "users",
        "country",
        ["country_code"],
        ["id"],
    )
    op.drop_column("users", "country")


def downgrade() -> None:
    op.add_column(
        "users",
        sa.Column(
            "country",
            sa.VARCHAR(length=50),
            autoincrement=False,
            nullable=False,
        ),
    )
    op.drop_constraint(
        op.f("fk_users_country_code_country"),
        "users",
        type_="foreignkey",
    )
    op.drop_column("users", "country_code")
    op.drop_table("country")
