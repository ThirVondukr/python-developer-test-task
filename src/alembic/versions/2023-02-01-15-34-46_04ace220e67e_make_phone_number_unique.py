"""Make phone number unique

Revision ID: 04ace220e67e
Revises: 1f5f58afe6fa
Create Date: 2023-02-01 15:34:46.940207

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "04ace220e67e"
down_revision = "1f5f58afe6fa"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.drop_index("ix_users_phone_number", table_name="users")
    op.create_index(
        op.f("ix_users_phone_number"),
        "users",
        ["phone_number"],
        unique=True,
    )


def downgrade() -> None:
    op.drop_index(op.f("ix_users_phone_number"), table_name="users")
    op.create_index("ix_users_phone_number", "users", ["phone_number"], unique=False)
