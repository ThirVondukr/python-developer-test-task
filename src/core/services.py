import random

from fastapi import Depends
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from core.dto import UserCreateDto
from db.dependencies import get_session
from db.models import Country, User


class CountryService:
    def __init__(self, session: AsyncSession = Depends(get_session)) -> None:
        self._session = session

    async def suggest_country(self, country_name: str) -> Country:
        stmt = select(Country).where(Country.name == country_name)
        country = (await self._session.execute(stmt)).scalar()
        if country is not None:
            return country

        country_code = random.randint(1, 1000)  # Could be a remote service call
        return await self._session.get(Country, country_code) or await self._create(
            code=country_code,
            name=country_name,
        )

    async def _create(self, code: int, name: str) -> Country:
        country = Country(id=code, name=name)
        self._session.add(country)
        await self._session.flush()
        return country


class UserService:
    def __init__(self, session: AsyncSession = Depends(get_session)) -> None:
        self._session = session

    async def retrieve(self, phone_number: str) -> User | None:
        stmt = (
            select(User)
            .where(User.phone_number == phone_number)
            .options(joinedload(User.country))
        )
        return (await self._session.execute(stmt)).scalar()

    async def create_or_update(self, dto: UserCreateDto) -> User:
        stmt = select(User).where(User.phone_number == dto.phone_number)
        existing_user = (await self._session.execute(stmt)).scalar()
        user = existing_user or User()

        user.name = dto.name
        user.surname = dto.surname
        user.patronymic = dto.patronymic
        user.phone_number = dto.phone_number
        user.email = dto.email
        user.country = dto.country

        self._session.add(user)
        await self._session.flush()
        return user

    async def delete(self, phone_number: str) -> None:
        stmt = delete(User).where(User.phone_number == phone_number)
        await self._session.execute(stmt)
