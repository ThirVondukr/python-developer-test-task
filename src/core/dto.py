import dataclasses

from pydantic import BaseModel

from db.models import Country


class BaseDto(BaseModel):
    class Config:
        orm_mode = True


@dataclasses.dataclass
class UserCreateDto:
    name: str
    surname: str
    patronymic: str | None
    phone_number: str
    email: str | None
    country: Country
