import re
from collections.abc import Callable, Iterator
from typing import Any

cyrillic_str_regex = r"^[а-яА-Я\- ]+$"


class PhoneNumber(str):
    _regex = re.compile(r"^7\d+$")
    _max_length = 12

    @classmethod
    def __get_validators__(cls) -> Iterator[Callable[..., Any]]:
        yield cls.validate

    @classmethod
    def __modify_schema__(cls, field_schema: dict[str, Any]) -> None:
        field_schema.update(
            pattern=cls._regex.pattern,
            max_length=cls._max_length,
        )

    @classmethod
    def validate(cls, value: Any) -> str:  # noqa: ANN401
        if not isinstance(value, str):
            raise TypeError

        error = ValueError("Invalid phone number")
        if len(value) > cls._max_length:
            raise error

        m = cls._regex.fullmatch(value)
        if not m:
            raise error
        return cls(m.group(0))
